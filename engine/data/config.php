<?PHP 

//System Configurations

$config = array (

'home_title' => 'Новостной портал СНГ:: ИА «Тирас»',

'http_home_url' => 'https://tiras.ru/',

'charset' => 'utf-8',

'description' => 'Новости Украины, Приднестровья и Молдовы; события в политике, обществе, спорте. Сводка происшествий. Интервью.',

'keywords' => 'Новости, Тирасполь, Приднестровье, ПМР, Украины, Одесса, Экономика, Общество, События, Соин, Transnistria, PMR, Tiraspol, Moldova, conflict, war 1992',

'short_title' => 'ИА «Тирас»',

'start_site' => '1',

'date_adjust' => '-60',

'allow_alt_url' => 'yes',

'seo_type' => '2',

'seo_control' => '0',

'langs' => 'Russian',

'skin' => 'Full',

'allow_admin_wysiwyg' => '1',

'allow_static_wysiwyg' => '1',

'allow_complaint_mail' => '0',

'site_offline' => 'no',

'offline_reason' => 'Сайт находится на текущей реконструкции, после завершения всех работ сайт будет открыт.<br /><br />Приносим вам свои извинения за доставленные неудобства.',

'admin_path' => 'admin.php',

'extra_login' => '0',

'login_log' => '3',

'ip_control' => '1',

'log_hash' => '1',

'sec_addnews' => '2',

'spam_api_key' => '',

'allow_recaptcha' => '0',

'recaptcha_public_key' => '6LfoOroSAAAAAEg7PViyas0nRqCN9nIztKxWcDp_',

'recaptcha_private_key' => '6LfoOroSAAAAAMgMr_BTRMZy20PFir0iGT2OQYZJ',

'recaptcha_theme' => 'clean',

'news_number' => '10',

'search_number' => '10',

'related_number' => '3',

'top_number' => '10',

'tags_number' => '10',

'max_moderation' => '0',

'news_restricted' => '0',

'smilies' => 'wink,winked,smile,am,belay,feel,fellow,laughing,lol,love,no,recourse,request,sad,tongue,wassat,crying,what,bully,angry',

'timestamp_active' => 'H:i, j F Y',

'news_navigation' => '1',

'news_sort' => 'date',

'news_msort' => 'DESC',

'catalog_sort' => 'date',

'catalog_msort' => 'DESC',

'image_align' => 'left',

'news_future' => '0',

'create_catalog' => '0',

'parse_links' => '0',

'mail_news' => '0',

'show_sub_cats' => '1',

'short_rating' => '0',

'hide_full_link' => 'no',

'allow_search_print' => '0',

'allow_add_tags' => '0',

'allow_share' => '1',

'allow_site_wysiwyg' => '1',

'allow_quick_wysiwyg' => '1',

'allow_comments' => 'no',

'comments_restricted' => '0',

'allow_subscribe' => '0',

'allow_combine' => '0',

'max_comments_days' => '0',

'comments_minlen' => '10',

'comments_maxlen' => '300',

'comm_nummers' => '30',

'comm_msort' => 'ASC',

'flood_time' => '30',

'auto_wrap' => '80',

'timestamp_comment' => 'j F Y H:i',

'allow_search_link' => '0',

'mail_comments' => '0',

'allow_comments_wysiwyg' => '1',

'allow_cache' => 'yes',

'clear_cache' => '0',

'cache_type' => '0',

'memcache_server' => 'localhost:11211',

'allow_comments_cache' => '0',

'allow_gzip' => 'no',

'js_min' => '1',

'full_search' => '0',

'fast_search' => '0',

'allow_registration' => 'no',

'allow_multi_category' => '0',

'related_news' => '1',

'no_date' => '1',

'allow_fixed' => '1',

'speedbar' => '1',

'allow_banner' => '1',

'allow_cmod' => '0',

'allow_votes' => 'yes',

'allow_topnews' => 'yes',

'allow_read_count' => '2',

'cache_count' => '0',

'allow_calendar' => 'yes',

'allow_archives' => 'yes',

'rss_informer' => '0',

'allow_tags' => '0',

'allow_change_sort' => '0',

'comments_ajax' => '0',

'online_status' => '0',

'files_allow' => 'yes',

'max_file_count' => '0',

'files_force' => '1',

'files_antileech' => '1',

'files_count' => 'yes',

'admin_mail' => 'tiras.ru@yandex.ru',

'mail_title' => '',

'mail_metod' => 'php',

'mail_additional' => '',

'smtp_host' => 'localhost',

'smtp_port' => '25',

'smtp_user' => '',

'smtp_pass' => '',

'smtp_mail' => '',

'use_admin_mail' => '0',

'smtp_helo' => 'HELO',

'mail_bcc' => '0',

'auth_metod' => '0',

'reg_group' => '5',

'registration_type' => '1',

'reg_multi_ip' => '1',

'auth_domain' => '1',

'registration_rules' => '1',

'allow_sec_code' => 'yes',

'reg_question' => '0',

'allow_skin_change' => 'yes',

'mail_pm' => '1',

'max_users' => '6',

'max_users_day' => '0',

'max_up_side' => '0',

'o_seite' => '0',

'max_up_size' => '8000',

'max_image_days' => '999',

'allow_watermark' => 'no',

'max_watermark' => '150',

'max_image' => '500',

't_seite' => '2',

'jpeg_quality' => '100',

'avatar_size' => '100',

'tag_img_width' => '0',

'thumb_dimming' => '1',

'thumb_gallery' => '1',

'outlinetype' => '1',

'allow_smartphone' => '0',

'allow_smart_images' => '1',

'allow_smart_video' => '1',

'allow_smart_format' => '1',

'mobile_news' => '10',

'allow_rss' => '1',

'rss_mtype' => '1',

'rss_number' => '20',

'rss_format' => '2',

'version_id' => '10.0',

'key' => 'fecdd870fcf2cb4bfc0b460c4b04fb62',

);

?>