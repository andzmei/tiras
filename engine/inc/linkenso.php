<?php

/*
=============================================================================
 Файл: linkenso.php (backend) версия 1.4
-----------------------------------------------------------------------------
 Автор: Фомин Александр Алексеевич, mail@mithrandir.ru
-----------------------------------------------------------------------------
 Назначение: генератор кода для вставки модуля в шаблон fullstory.tpl
=============================================================================
*/

    // Антихакер
    if( !defined( 'DATALIFEENGINE' ) OR !defined( 'LOGGED_IN' ) ) {
            die( "Hacking attempt!" );
    }

    echoheader('linkenso', 'Генератор кода для вставки модуля в шаблон');
        echo '

'.($config['version_id'] >= 10.2 ? '<style>.uniform, div.selector {min-width: 250px;}</style>' : '<style>
@import url("engine/skins/application.css");

.box {
margin:10px;
}
.uniform {
position: relative;
padding-left: 5px;
overflow: hidden;
min-width: 250px;
font-size: 12px;
-webkit-border-radius: 0;
-moz-border-radius: 0;
-ms-border-radius: 0;
-o-border-radius: 0;
border-radius: 0;
background: whitesmoke;
background-image: url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgi…pZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==");
background-size: 100%;
background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #ffffff), color-stop(100%, #f5f5f5));
background-image: -webkit-linear-gradient(top, #ffffff, #f5f5f5);
background-image: -moz-linear-gradient(top, #ffffff, #f5f5f5);
background-image: -o-linear-gradient(top, #ffffff, #f5f5f5);
background-image: linear-gradient(top, #ffffff, #f5f5f5);
-webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
-moz-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
border: 1px solid #ccc;
font-size: 12px;
height: 28px;
line-height: 28px;
color: #666;
}
</style>').'

<div class="box">

	<div class="box-header">
		<div class="title">Генератор кода для вставки модуля</div>
		<ul class="box-toolbar">
			<li class="toolbar-link">
			<a target="_blank" href="http://alaev.info/post/3322?from=LinkEnso">LinkEnso v.1.4 © 2014 Блог АлаичЪ\'а - разработка и поддержка модуля</a>
			</li>
		</ul>
	</div>

	<div class="box-content">
	<table class="table table-normal">
	<tbody>
		<tr>
		<td class="col-xs-6"><h5>Количество ссылок:</h5><span class="note large">Общее количество ссылок, выводимых модулем.</span></td>
		<td class="col-xs-6 settingstd"><input class="uniform" type="text" name="linkenso_links" id="linkenso_links" value="3" /></td>
		</tr><tr>
		<td class="col-xs-6"><h5>Какие новости показывать:</h5><span class="note large">Опция для отображения свежих или предыдущих постов.<br /><strong>предыдущие новости</strong> - В ссылках будут выведены предыдущие новости.<br /><strong>свежие новости</strong> - В ссылках будут выведены свежие новости.</span></td>
		<td class="col-xs-6 settingstd">
			<select class="uniform" name="linkenso_date" id="linkenso_date">
			<option value="old">предыдущие новости</option>
			<option value="new">свежие новости</option>
			</select>
		</td>
		</tr><tr>
		<td class="col-xs-6"><h5>Закольцевать ссылки:</h5><span class="note large">Закольцевать ли ссылки.<br /><strong>да</strong> - Ссылки будут закольцованы, т.к. в блоке свежих статей  в последних новостях будут отображены самые первые новости.<br /><strong>нет</strong> - Ссылки не будут закольцованы, если не будет найдено свежих или предыдущих ссылок, модуль ничего не выведет.</span></td>
		<td class="col-xs-6 settingstd">
			<select class="uniform" name="linkenso_ring" id="linkenso_ring">
			<option value="yes">да</option>
			<option value="no">нет</option>
			</select>
		</td>
		</tr><tr>
		<td class="col-xs-6"><h5>Сканировать категории:</h5><span class="note large">Глубина сканирования категорий для вывода ссылок.<br /><strong>все категории</strong> - В модуле будут выводиться ссылки на новости из всех категорий.<br /><strong>текущая категория</strong> - В модуле будут выводиться ссылки на новости из той же категории, что и текущая.<br /><strong>глобальная категория</strong> - В модуле будут выводиться ссылки на новости из самой корневой категории для текущей новости.</span></td>
		<td class="col-xs-6 settingstd">
			<select class="uniform" name="linkenso_scan" id="linkenso_scan">
			<option value="all_cat">все категории</option>
			<option value="same_cat">текущая категория</option>
			<option value="global_cat">глобальная категория</option>
			</select>
		</td>
		</tr><tr>
		<td class="col-xs-6"><h5>Анкор:</h5><span class="note large">Принцип вывода анкора ссылки.<br /><strong>название новости</strong> - В ссылках будут выведены заголовки новостей.<br /><strong>title новости</strong> - В ссылках будут выведены title новостей.</span></td>
		<td class="col-xs-6 settingstd">
			<select class="uniform" name="linkenso_anchor" id="linkenso_anchor">
			<option value="name">название новости</option>
			<option value="title">title новости</option>
			</select>
		</td>
		</tr><tr>
		<td class="col-xs-6"><h5>Атрибут title ссылок:</h5><span class="note large">Принцип вывода атрибута title ссылки.<br /><strong>title новости</strong> - В title будут выведены title новостей.<br /><strong>название новости</strong> - В title будут выведены заголовки новостей.</span></td>
		<td class="col-xs-6 settingstd">
			<select class="uniform" name="linkenso_title" id="linkenso_title">
			<option value="title">title новости</option>
			<option value="name">название новости</option>
			<option value="empty">оставить пустым</option>
			</select>
		</td>
		</tr><tr>
		<td class="col-xs-6"><h5>Код для вставки в <strong>fullstory.tpl</strong></h5><span class="note large">Вставлять код необходимо в то место шаблона, где вы хотите видеть будущий блок ссылок.</span></td>
		<td class="col-xs-6 settingstd">
			<textarea type="text" style="width:100%;height:100px;" name="linkenso_code" id="linkenso_code" >{include file=\'engine/modules/linkenso.php?post_id={news-id}&links=3&date=old&ring=yes&scan=all_cat&anchor=name&title=title\'}</textarea>
		</td>
		</tr>


                                        


                                <script type="text/javascript">
                                    document.getElementById("linkenso_links").onchange = function(){recalculate_code()};
                                    document.getElementById("linkenso_date").onchange = function(){recalculate_code()};
                                    document.getElementById("linkenso_ring").onchange = function(){recalculate_code()};
                                    document.getElementById("linkenso_scan").onchange = function(){recalculate_code()};
                                    document.getElementById("linkenso_anchor").onchange = function(){recalculate_code()};
                                    document.getElementById("linkenso_title").onchange = function(){recalculate_code()};
                                        
                                    function recalculate_code()
                                    {
                                        var linkenso_links = document.getElementById("linkenso_links").value;
                                        var linkenso_date = document.getElementById("linkenso_date").value;
                                        var linkenso_ring = document.getElementById("linkenso_ring").value;
                                        var linkenso_scan = document.getElementById("linkenso_scan").value;
                                        var linkenso_anchor = document.getElementById("linkenso_anchor").value;
                                        var linkenso_title = document.getElementById("linkenso_title").value;
                                        document.getElementById("linkenso_code").value = "{include file=\'engine/modules/linkenso.php?post_id={news-id}&links="+linkenso_links+"&date="+linkenso_date+"&ring="+linkenso_ring+"&scan="+linkenso_scan+"&anchor="+linkenso_anchor+"&title="+linkenso_title+"\'}";
                                    }
                                </script>
	</tbody>
	</table>
	</div>
</div>
        ';

        // Отображение подвала админского интерфейса
        echofooter();
  
?>