<?php
/*
=====================================================
http://www.text-link-ads.com/
-----------------------------------------------------
Copyright (c) 2011 Text Link Ads
=====================================================
This code is protected by copyright
=====================================================
File: textlinkads.php
-----------------------------------------------------
Purpose:Serve TLA ads in datalife
=====================================================
*/

if (!defined('DATALIFEENGINE') OR !defined('LOGGED_IN')) {
    die('Hacking attempt!');
}

if (!$user_group[$member_id['user_group']]['admin_banners']) {
    msg("error", $lang['index_denied'], $lang['index_denied']);
}

if (isset($_REQUEST['id'])) {
    $id = intval($_REQUEST['id']);
} else {
    $id = '';
}

checkInstall();
function checkInstall()
{
    global $db;
    $db->query("SHOW TABLES LIKE '" . PREFIX . "_textlinkads'");
    if (!$row = $db->get_row()) {     
       $sql = "CREATE TABLE " . PREFIX . "_textlinkads (
          `id` int(10) NOT NULL AUTO_INCREMENT,
          `xml_key` varchar(255) default '',
          `url` varchar(255) default '',
          PRIMARY KEY (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $db->query($sql);
    }
}

if ($_POST['action'] == 'doadd') {
    if ($_REQUEST['user_hash'] == "" or $_REQUEST['user_hash'] != $dle_login_hash) {
        die('Hacking attempt! User not found');
    }
    
    $xml_key = totranslit(strip_tags(trim($_POST['xml_key'])));
    $url = $db->safesql(strip_tags(trim($_POST['url'])));
    
    if ($xml_key == '' or $url == '') {
        msg("error adding xml key or url", "javascript:history.go(-1)");
    }
    
    $db->query("INSERT INTO " . PREFIX . "_textlinkads (xml_key,url) VALUES ('$xml_key', '$url')");
    @unlink(ENGINE_DIR . '/cache/system/textlinkads.php');
    clear_cache();
    header("Location: " . $_SERVER['PHP_SELF'] . "?mod=textlinkads");
}

if ($_POST['action'] == 'doedit') {
    if ($_REQUEST['user_hash'] == '' or $_REQUEST['user_hash'] != $dle_login_hash) {
        die('Hacking attempt! User not found');
    }

    if (!$id) {
        msg("error", "ID not valid", "ID not valid");
    }
    
    $xml_key = totranslit(strip_tags(trim($_POST['xml_key'])));
    $url = $db->safesql(strip_tags(trim($_POST['url'])));
    
    if ($xml_key == '' or $url == '') {
        msg("error updatin xml key or url", "javascript:history.go(-1)");
    }
    
    $db->query("UPDATE " . PREFIX . "_textlinkads SET xml_key='$xml_key', url='$url' WHERE id='$id'");
    @unlink(ENGINE_DIR . '/cache/system/textlinkads.php');
    clear_cache();
    header("Location: " . $_SERVER['PHP_SELF'] . "?mod=textlinkads");
}

if ($_GET['action'] == 'delete') {
    if ($_REQUEST['user_hash'] == '' or $_REQUEST['user_hash'] != $dle_login_hash) {
        die('Hacking attempt! User not found');
    }

    if (!$id) {
        msg("error", "ID not valid", "ID not valid");
    }
    
    $db->query("DELETE FROM " . PREFIX . "_textlinkads WHERE id='$id'");
    @unlink(ENGINE_DIR . '/cache/system/textlinkads.php');
    clear_cache();
}

if ($_REQUEST['action'] == 'add' or $_REQUEST['action'] == 'edit') {
    $start_date = '';
    $stop_date  = '';

    $js_array[] = 'engine/skins/calendar.js';
    $js_array[] = 'engine/skins/calendar-en.js';
    $js_array[] = 'engine/skins/calendar-setup.js';

    if ($_REQUEST['action'] == 'add') {
        $checked = 'checked';
        $doaction = 'doadd';
        $all_cats = 'selected';
        $check_all = 'selected';
        $groups = get_groups();
    } else {
        $row = $db->super_query("SELECT * FROM " . PREFIX . "_textlinkads WHERE id='$id' LIMIT 0,1");
        $xml_key = $row['xml_key'];
        $url = htmlspecialchars(stripslashes($row['url']));
        $doaction = 'doedit';
        
        $groups = get_groups(explode(',', $row['grouplevel']));
        if ($row['grouplevel'] == 'all') {
            $check_all = 'selected';
        } else {
            $check_all = '';
        }
    }
    
    echoheader("", "");
    
    echo <<<HTML
<link rel="stylesheet" type="text/css" media="all" href="engine/skins/calendar-blue.css" title="win2k-cold-1" />
    <form action="" method="post" name="textlinkadsform">
      <input type="hidden" name="mod" value="textlinkads">
      <input type="hidden" name="action" value="{$doaction}">
<table width="100%">
    <tr>
        <td width="4"><img src="engine/skins/images/tl_lo.gif" width="4" height="4" border="0"></td>
        <td background="engine/skins/images/tl_oo.gif"><img src="engine/skins/images/tl_oo.gif" width="1" height="4" border="0"></td>
        <td width="6"><img src="engine/skins/images/tl_ro.gif" width="6" height="4" border="0"></td>
    </tr>
    <tr>
        <td background="engine/skins/images/tl_lb.gif"><img src="engine/skins/images/tl_lb.gif" width="4" height="1" border="0"></td>
        <td style="padding:5px;" bgcolor="#FFFFFF">
<table width="100%">
    <tr>
        <td bgcolor="#EFEFEF" height="29" style="padding-left:10px;"><div class="navigation">Text Link Ads</div></td>
    </tr>
</table>
<div class="unterline"></div>
<table width="100%">
    <tr>
        <td width="200" style="padding:4px;">XML KEY</td>
        <td><input class="edit bk" style="width: 312px;" type="text" name="xml_key" value="{$xml_key}" />&nbsp;&nbsp;&nbsp;(submit sites through text-link-ads.com to get xml keys.)</td>
    </tr>
    <tr>
        <td style="padding:4px;">URI</td>
        <td><input  class="edit bk" style="width: 312px;" type="text" name="url" value="{$URL}" /></td>
    </tr>
    <tr>
        <td colspan=2><div class="hr_line"></div></td>
    </tr>
    <tr>
        <td colspan=2 style="padding:4px;"><input type="submit" class="buttons" value="&nbsp;&nbsp;{$lang['user_save']}&nbsp;&nbsp;"></td>
    </tr>
</table>
</td>
        <td background="engine/skins/images/tl_rb.gif"><img src="engine/skins/images/tl_rb.gif" width="6" height="1" border="0"></td>
    </tr>
    <tr>
        <td><img src="engine/skins/images/tl_lu.gif" width="4" height="6" border="0"></td>
        <td background="engine/skins/images/tl_ub.gif"><img src="engine/skins/images/tl_ub.gif" width="1" height="6" border="0"></td>
        <td><img src="engine/skins/images/tl_ru.gif" width="6" height="6" border="0"></td>
    </tr>
</table>
<input type="hidden" name="user_hash" value="$dle_login_hash" />
    </form>
HTML;
    
    echofooter();

} else {
    $js_array[] = 'engine/classes/js/menu.js';  
    echoheader("", "");
    
    $db->query("SELECT * FROM " . PREFIX . "_textlinkads ORDER BY id DESC");
    
    $entries = '';
    
    while ($row = $db->get_row()) {
        $row['xml_key'] = stripslashes($row['xml_key']);
        $row['url'] = stripslashes($row['url']);
        $entries .= "
   <tr>
    <td height=22 class=\"list\">{$row['xml_key']}</td>
    <td>{$row['url']}</td>
    <td class=\"list\" align=\"center\"><a onClick=\"return dropdownmenu(this, event, MenuBuild('" . $row['id'] . "', '" . $led_action . "'), '150px')\" href=\"#\"><img src=\"engine/skins/images/browser_action.gif\" border=\"0\"></a></td>
     </tr>";
    }
    $db->free();
    
    echo <<<HTML
<script type="text/javascript">
<!--
function MenuBuild(m_id , led_action) {

var menu=new Array()
var lang_action = '';

menu[0]='<a onClick="document.location=\'?mod=textlinkads&user_hash={$dle_login_hash}&action=' + led_action + '&id=' + m_id + '\'; return(false)" href="#">' + lang_action + '</a>';
menu[1]='<a onClick="document.location=\'?mod=textlinkads&user_hash={$dle_login_hash}&action=edit&id=' + m_id + '\'; return(false)" href="#">{$lang['group_sel1']}</a>';
menu[2]='<a onClick="javascript:confirmdelete(' + m_id + '); return(false)" href="#">{$lang['cat_del']}</a>';

return menu;
}
function confirmdelete(id){
    var agree=confirm("Are You sure you want to remove this Ad.");
    if (agree)
     document.location="?mod=textlinkads&action=delete&user_hash={$dle_login_hash}&id="+id;
}
//-->
</script>
<div style="padding-top:5px;padding-bottom:2px;">
<table width="100%">
    <tr>
        <td width="4"><img src="engine/skins/images/tl_lo.gif" width="4" height="4" border="0"></td>
        <td background="engine/skins/images/tl_oo.gif"><img src="engine/skins/images/tl_oo.gif" width="1" height="4" border="0"></td>
        <td width="6"><img src="engine/skins/images/tl_ro.gif" width="6" height="4" border="0"></td>
    </tr>
    <tr>
        <td background="engine/skins/images/tl_lb.gif"><img src="engine/skins/images/tl_lb.gif" width="4" height="1" border="0"></td>
        <td style="padding:5px;" bgcolor="#FFFFFF">
<table width="100%">
    <tr>
        <td bgcolor="#EFEFEF" height="29" style="padding-left:10px;"><div class="navigation">Text Link Ads Placment</div></td>
    </tr>
</table>
<div class="unterline"></div>

<table width="100%">
  <tr>
   <td width=150>XML KEY</td>
   <td align="center">URL</td>
  </tr>
    <tr><td colspan="5"><div class="hr_line"></div></td></tr>
    {$entries}
    <tr><td colspan="5"><div class="hr_line"></div></td></tr>
    <tr><td colspan="5" align="right">Login to <a href="http://www.text-link-ads.com">Text Link Ads</a> to get your site key and urls for this datalife site. <br /> To place your ads in your page add  {include file="engine/modules/textlinkads.php"} to the .tpl where you want your ads to display.</td></tr>
    <tr><td colspan="5"><a href="?mod=textlinkads&action=add"><input onclick="document.location='?mod=textlinkads&action=add'" type="button" class="buttons" value="&nbsp;&nbsp;Add URI and XML KEY&nbsp;&nbsp;"></a></td></tr>
</table>

</td>
        <td background="engine/skins/images/tl_rb.gif"><img src="engine/skins/images/tl_rb.gif" width="6" height="1" border="0"></td>
    </tr>
    <tr>
        <td><img src="engine/skins/images/tl_lu.gif" width="4" height="6" border="0"></td>
        <td background="engine/skins/images/tl_ub.gif"><img src="engine/skins/images/tl_ub.gif" width="1" height="6" border="0"></td>
        <td><img src="engine/skins/images/tl_ru.gif" width="6" height="6" border="0"></td>
    </tr>
</table>
</div>
HTML;
    
    echofooter();
}
?>
