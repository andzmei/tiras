<?php

if (!defined('DATALIFEENGINE')) {
    die("Hacking attempt!");
}

function get_news($cat_id, $cat_name, $cat_altname, $db) {
    $html = '';
    $sql = "select a.id,a.xfields,a.short_story,a.title,a.alt_name,a.date from " . PREFIX . "_post a where a.category=$cat_id order by a.date desc limit 6";
    $db->query($sql);
    $k = 1;
    while ($row = $db->get_row()) {

        $arrXF = explode('||', $row["xfields"]);
        foreach ($arrXF as $XF) {
            $NV = explode('|', $XF);
            if ($NV[0] == 'img') {
                $IMG = $NV[1];
            }
        }


        if ($k == 1) {
            preg_match("|<img(.*?)>|is", stripcslashes($row['full_story']), $preg);
            $html.='<div class="razdel">
      <div class="razdel-t">' . $cat_name . '</div>
      <div class="news-cat">
        <h1 class="title"><a href="/' . $cat_altname . '/' . $row["id"] . '-' . $row["alt_name"] . '.html">' . stripcslashes($row["title"]) . '</a></h1>
        ' . $preg[0] . '
            '.$IMG.'
        <p>' . stripcslashes($row["short_story"]) . '</p>
		
	<div class="data">' . $row["date"] . ' </div>  
    
        <div class="podrob" ><a href="/' . $cat_altname . '/' . $row["id"] . '-' . $row["alt_name"] . '.html">подробнее »</a></div>
      </div>
	  <div class="spisok">
        <ul>';
        } else {
            $html.='<li><a href="/' . $cat_altname . '/' . $row["id"] . '-' . $row["alt_name"] . '.html">' . stripcslashes($row["title"]) . '</a></li>';
        }

        $k = 2;
    }
    $html.='</ul>
      </div>
    </div>';
    $db->free();
    return($html);
}

$cat_news = dle_cache("cat_news", $config['skin']);
//$cat_news = $dle_api->load_from_cache("cat_news");
if ($cat_news === false) {

    $cat_news.=get_news(4, "Эксперты", 'jeksperty', $db);
    $cat_news.=get_news(1123, "Разговор без цензуры", 'honesty', $db);
    $cat_news.=get_news(7, "В мире", 'v-mire', $db);
    $cat_news.=get_news(5, "Евразия", 'evrazija', $db);
    $cat_news.=get_news(12, "Общество", 'obshhestvo', $db);
    $cat_news.=get_news(6, "Бизнес", 'biznes', $db);
    $cat_news.=get_news(8, "Военное", 'voennoe', $db);
    $cat_news.=get_news(11, "Образование", 'obrazovanie', $db);
    $cat_news.=get_news(9, "Религия", 'religija', $db);
    $cat_news.=get_news(10, "Здоровье", 'zdorove', $db);
    $cat_news.=get_news(13, "Спорт", 'sport', $db);
    $cat_news.=get_news(1122, "Криминальное чтиво", 'kriminalnoe-chtivo', $db);

    $db->free();
    $tpl->result['cat_news'] = $cat_news;
    create_cache("cat_news", $cat_news, $config['skin']);
    // $dle_api->save_to_cache("cat_news", $cat_news);
}
?>