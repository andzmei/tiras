<?php
/*
=====================================================
 DataLife Engine - by SoftNews Media Group 
-----------------------------------------------------
 http://dle-news.ru/
=====================================================
 Updated by Elegant Division (http://e-div.com)
-----------------------------------------------------
 v.1.5
=====================================================
*/

@session_start();

@error_reporting ( E_ALL ^ E_WARNING ^ E_NOTICE );
@ini_set ( 'display_errors', true );
@ini_set ( 'html_errors', false );
@ini_set ( 'error_reporting', E_ALL ^ E_WARNING ^ E_NOTICE );

define( 'DATALIFEENGINE', true );
define( 'ROOT_DIR', substr( dirname(  __FILE__ ), 0, -25 ) );
define( 'ENGINE_DIR', ROOT_DIR . '/engine' );

include ENGINE_DIR . '/data/config.php';

if( $config['http_home_url'] == "" ) {
	
	$config['http_home_url'] = explode( "engine/modules/edCalendar/ajaxHorizontal.php", $_SERVER['PHP_SELF'] );
	$config['http_home_url'] = reset( $config['http_home_url'] );
	$config['http_home_url'] = "http://" . $_SERVER['HTTP_HOST'] . $config['http_home_url'];

}

require_once ENGINE_DIR . '/classes/mysql.php';
require_once ENGINE_DIR . '/data/dbconfig.php';

$PHP_SELF = $config['http_home_url'] . "index.php";

require_once ENGINE_DIR . '/modules/functions.php';

$_COOKIE['dle_skin'] = trim(totranslit( $_COOKIE['dle_skin'], false, false ));

if( $_COOKIE['dle_skin'] ) {

	if( @is_dir( ROOT_DIR . '/templates/' . $_COOKIE['dle_skin'] ) ) {
		$config['skin'] = $_COOKIE['dle_skin'];
	}
}

if( $config["lang_" . $config['skin']] ) {

	if ( file_exists( ROOT_DIR . '/language/' . $config["lang_" . $config['skin']] . '/website.lng' ) ) {	
		@include_once ROOT_DIR . '/language/' . $config["lang_" . $config['skin']] . '/website.lng';
	} else die("Language file not found");

} else {
	
	@include_once ROOT_DIR . '/language/' . $config['langs'] . '/website.lng';

}

$config['charset'] = ($lang['charset'] != '') ? $lang['charset'] : $config['charset'];


# Генерируем календарь
function horcal($cal_month, $cal_year, $events) {
	global $f, $r, $year, $month, $config, $lang, $langdateshortweekdays, $PHP_SELF;

	$next = true;
	
	if( intval( $cal_year . $cal_month ) >= date( 'Ym' ) AND !$config['news_future'] ) $next = false;

	$cur_date=date( 'Ymj', time() + ($config['date_adjust'] * 60) );
	$cal_date = $cal_year.$cal_month;

	$cal_month = intval( $cal_month );
	$cal_year = intval( $cal_year );
	
	if( $cal_month < 0 ) $cal_month = 1;
	if( $cal_year < 0 ) $cal_year = 2008;
	
	$first_of_month = mktime( 0, 0, 0, $cal_month, 7, $cal_year );
	$maxdays = date( 't', $first_of_month ) + 1; // 28-31
	$prev_of_month = mktime( 0, 0, 0, ($cal_month - 1), 7, $cal_year );
	$next_of_month = mktime( 0, 0, 0, ($cal_month + 1), 7, $cal_year );
	$cal_day = 1;
	$weekday = date( 'w', $first_of_month ) + 1; // 0-6
	
	$buffer = '<table id="edTableCalendar"><tr>';

	// link to prev month
	$buffer .= '<td class="edMonthLink edPrevMonth" rowspan="2" data-month="' . date( 'm', $prev_of_month ) . '" data-year="' . date( 'Y', $prev_of_month ) . '" title="' . $lang['prev_moth'] . '"></td>';
	
	// month  + year
	$trueColpsan = $maxdays - 1;
	$buffer .= '<td class="edMonth" colspan="' . $trueColpsan . '">' . langdate( 'F', $first_of_month ) . ' ' . $cal_year . '</td>';
	
	// link to next month
	$buffer .= '<td class="edMonthLink edNextMonth' . ( !$next ? ' edNo' : '' ) . '" rowspan="2" data-month="' . date( 'm', $next_of_month ) . '" data-year="' . date( 'Y', $next_of_month ) . '" title="' . $lang['next_moth'] . '"></td></tr><tr>';
	
	$buffer = str_replace( $f, $r, $buffer );
	
	while ( $maxdays > $cal_day ) {
		if($weekday > 6 ) $weekday = 0;
		
		$wd = $cal_day . '<span>' . $langdateshortweekdays[$weekday] . '</span>'; // weekdays

		$cal_pos = $cal_date.$cal_day;
		
		# В данный день есть новость
		if( isset( $events[$cal_day] ) ) {
			$date['title'] = langdate( 'd F Y', $events[$cal_day] );
			
			if( $config['allow_alt_url'] == "yes" ) $callLink = $config['http_home_url'] . '' . date( "Y/m/d", $events[$cal_day] );
			else $callLink = $PHP_SELF . '?year=' . date( "Y", $events[$cal_day] ) . '&amp;month=' . date( "m", $events[$cal_day] ) . '&day=' . date( "d", $events[$cal_day] );
			
			$buffer .= '<td class="edActiveDay ' . (($weekday == '6' || $weekday == '0') ? 'edWeekEnd ' : '' ) . (($cal_pos==$cur_date) ? 'edCurrentDay' : '' ) . '"><a href="' . $callLink . '" title="' . $lang['cal_post'] . ' ' . $date['title'] . '">' . $wd . '</a></td>';
		}

		# В данный день новостей нет.
		else $buffer .= '<td class="' . (($weekday == '6' || $weekday == '0') ? 'edWeekEnd ' : '' ) . (($cal_pos==$cur_date) ? 'edCurrentDay' : '') . '">' . $wd . '</td>';
		
		$cal_day ++;
		$weekday ++;
	}
	
	return $buffer . '</tr></table>';
}

$buffer = false;
$time = time() + ($config['date_adjust'] * 60);
$thisdate = date( "Y-m-d H:i:s", $time );
if( $config['no_date'] AND !$config['news_future'] ) $where_date = " AND date < '" . $thisdate . "'"; else $where_date = "";

$this_month = date( 'm', $time );
$this_year = date( 'Y', $time );

$month = intval( $_GET['month'] );
$month = ($month<10?"0".$month:$month);
$year = intval( $_GET['year'] );
$sql = "";

if( $year != '' and $month != '' ) {
	
	if( ($year == $this_year and $month < $this_month) or ($year < $this_year) ) {

		$where_date = "";
		$approve = "";

	} else {
		$approve = " AND approve=1";
	}
	
	$sql = "SELECT DISTINCT DAYOFMONTH(date) as day FROM " . PREFIX . "_post WHERE date >= '{$year}-{$month}-01' AND date < '{$year}-{$month}-01' + INTERVAL 1 MONTH" . $approve . $where_date;
	
	
	$this_month = $month;
	$this_year = $year;

} else {
	
	$sql = "SELECT DISTINCT DAYOFMONTH(date) as day FROM " . PREFIX . "_post WHERE date >= '{$this_year}-{$this_month}-01' AND date < '{$this_year}-{$this_month}-01' + INTERVAL 1 MONTH AND approve=1" . $where_date;

}

if( $sql != "" ) {
	
	$db->query( $sql );
	
	while ( $row = $db->get_row() ) {
		$events[$row['day']] = strtotime( $this_year . "-" . $this_month . "-" . $row['day'] );
	}
	
	$db->free();
}
$db->close();

$buffer = horcal( $this_month, $this_year, $events );

header( "Content-type: text/html; charset=" . $config['charset'] );
echo $buffer;

?>