<?php
/*
=====================================================
 DataLife Engine - by SoftNews Media Group 
-----------------------------------------------------
 http://dle-news.ru/
=====================================================
 Updated by Elegant Division (http://e-div.com)
-----------------------------------------------------
 v.1.5
=====================================================
*/

if( ! defined( 'DATALIFEENGINE' ) ) {
	die( "Hacking attempt!" );
}

$is_change = false;

if ($config['allow_cache'] != "yes") { $config['allow_cache'] = "yes"; $is_change = true;}

# Генерируем календарь
function horcal($cal_month, $cal_year, $events) {
	global $f, $r, $year, $month, $config, $lang, $langdateshortweekdays, $PHP_SELF;

	$next = true;
	
	if( intval( $cal_year . $cal_month ) >= date( 'Ym' ) AND !$config['news_future'] ) $next = false;

	$cur_date=date( 'Ymj', time() + ($config['date_adjust'] * 60) );
	$cal_date = $cal_year.$cal_month;

	$cal_month = intval( $cal_month );
	$cal_year = intval( $cal_year );
	
	if( $cal_month < 0 ) $cal_month = 1;
	if( $cal_year < 0 ) $cal_year = 2008;
	
	$first_of_month = mktime( 0, 0, 0, $cal_month, 7, $cal_year );
	$maxdays = date( 't', $first_of_month ) + 1; // 28-31
	$prev_of_month = mktime( 0, 0, 0, ($cal_month - 1), 7, $cal_year );
	$next_of_month = mktime( 0, 0, 0, ($cal_month + 1), 7, $cal_year );
	$cal_day = 1;
	$weekday = date( 'w', $first_of_month ) + 1; // 0-6
	
	$buffer = '<table id="edTableCalendar"><tr>';

	// link to prev month
	$buffer .= '<td class="edMonthLink edPrevMonth" rowspan="2" data-month="' . date( 'm', $prev_of_month ) . '" data-year="' . date( 'Y', $prev_of_month ) . '" title="' . $lang['prev_moth'] . '"></td>';
	
	// month  + year
	$trueColpsan = $maxdays - 1;
	$buffer .= '<td class="edMonth" colspan="' . $trueColpsan . '">' . langdate( 'F', $first_of_month ) . ' ' . $cal_year . '</td>';

	// link to next month
	$buffer .= '<td class="edMonthLink edNextMonth' . ( !$next ? ' edNo' : '' ) . '" rowspan="2" data-month="' . date( 'm', $next_of_month ) . '" data-year="' . date( 'Y', $next_of_month ) . '" title="' . $lang['next_moth'] . '"></td></tr><tr>';
	
	$buffer = str_replace( $f, $r, $buffer );
	
	while ( $maxdays > $cal_day ) {
		if($weekday > 6 ) $weekday = 0;
		
		$wd = $cal_day . '<span>' . $langdateshortweekdays[$weekday] . '</span>'; // weekdays

		$cal_pos = $cal_date.$cal_day;
		
		# В данный день есть новость
		if( isset( $events[$cal_day] ) ) {
			$date['title'] = langdate( 'd F Y', $events[$cal_day] );
			
			if( $config['allow_alt_url'] == "yes" ) $callLink = $config['http_home_url'] . '' . date( "Y/m/d", $events[$cal_day] );
			else $callLink = $PHP_SELF . '?year=' . date( "Y", $events[$cal_day] ) . '&amp;month=' . date( "m", $events[$cal_day] ) . '&day=' . date( "d", $events[$cal_day] );
			
			$buffer .= '<td class="edActiveDay ' . (($weekday == '6' || $weekday == '0') ? 'edWeekEnd ' : '' ) . (($cal_pos==$cur_date) ? 'edCurrentDay' : '' ) . '"><a href="' . $callLink . '" title="' . $lang['cal_post'] . ' ' . $date['title'] . '">' . $wd . '</a></td>';
		}

		# В данный день новостей нет.
		else $buffer .= '<td class="' . (($weekday == '6' || $weekday == '0') ? 'edWeekEnd ' : '' ) . (($cal_pos==$cur_date) ? 'edCurrentDay' : '') . '">' . $wd . '</td>';
		
		$cal_day ++;
		$weekday ++;
	}
	
	return $buffer . '</tr></table>';
}
	
$events = array ();

$thisdate = date( "Y-m-d H:i:s", $_TIME );
if( $config['no_date'] AND !$config['news_future'] ) $where_date = " AND date < '" . $thisdate . "'";
else $where_date = "";

$this_month = date( 'm', $_TIME );
$this_year = date( 'Y', $_TIME );
$sql = "";

if( $year != '' and $month != '' ) $cache_id = $config['skin'] . $month . $year;
else $cache_id = $config['skin'] . $this_month . $this_year;

$echoCalendar = dle_cache( "horCalendar", $cache_id );

if( ! $echoCalendar ) {
	
	if( $year != '' and $month != '' ) {

		$month = totranslit($month, true, false);

		if( ($year == $this_year and $month < $this_month) or ($year < $this_year) ) {
			$where_date = "";
			$approve = "";
		} else {
			$approve = " AND approve=1";
		}
		
		$sql = "SELECT DISTINCT DAYOFMONTH(date) as day FROM " . PREFIX . "_post WHERE date >= '{$year}-{$month}-01' AND date < '{$year}-{$month}-01' + INTERVAL 1 MONTH" . $approve . $where_date;
		
		$this_month = $month;
		$this_year = $year;
	
	} else {
		
		$sql = "SELECT DISTINCT DAYOFMONTH(date) as day FROM " . PREFIX . "_post WHERE date >= '{$this_year}-{$this_month}-01' AND date < '{$this_year}-{$this_month}-01' + INTERVAL 1 MONTH AND approve=1" . $where_date;
	
	}
	
	if( $sql != "" ) {
		
		$db->query( $sql );
		
		while ( $row = $db->get_row() ) {
			$events[$row['day']] = strtotime( $this_year . "-" . $this_month . "-" . $row['day'] );
		}
		
		$db->free();
	}
	
	$echoCalendar = horcal( $this_month, $this_year, $events );
	create_cache( "horCalendar", $echoCalendar, $cache_id );
}

if ($is_change) $config['allow_cache'] = false;

echo "\n<!-- Calendar by Elegant Division -->\n";
echo $echoCalendar;
echo "\n<link href=\"" . $config['http_home_url'] . "engine/modules/edCalendar/media/style.css\" type=\"text/css\" rel=\"stylesheet\" />\n<script type=\"text/javascript\" src=\"" . $config['http_home_url'] . "engine/modules/edCalendar/media/js.js\"></script>";
echo "\n<!-- Calendar by Elegant Division -->\n";

?>