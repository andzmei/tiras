/* By Elegant Division (http://e-div.com) */

$("#edTableCalendar .edMonthLink:not(.edNo)").live("click", function(){
	$(this).parent().find(".edMonth").addClass("edLoading");
	$.get(dle_root + "engine/modules/edCalendar/ajaxHorizontal.php", { month: $(this).data('month'), year: $(this).data('year') }, function(data){
		$(this).parent().find(".edMonth").removeClass("edLoading");
		$("#edTableCalendar").html(data);
	});
});