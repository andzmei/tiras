<?php

/*
=============================================================================
 Файл: linkenso.php (frontend) версия 1.4
-----------------------------------------------------------------------------
 Автор: Фомин Александр Алексеевич, mail@mithrandir.ru
-----------------------------------------------------------------------------
 Назначение: кольцевая перелинковка новостей на сайте
=============================================================================
*/

    // Антихакер
    if( !defined( 'DATALIFEENGINE' )) {
            die( "Hacking attempt!" );
    }

    /*
     * Класс для вывода рубрик сайта
     */
    if(!class_exists('LinkEnso')) { class LinkEnso
    {
        /*
         * Конструктор класса LinkEnso - задаёт значение свойства dle_api и стандартных переменных
         * @param $linkEnsoConfig - массив с конфигурацией модуля
         */
        public function __construct($linkEnsoConfig)
        {
            // Подключаем DLE_API
            global $db, $config, $category;
            include ('engine/api/api.class.php');
            $this->dle_api = $dle_api;
            
            // Задаем конфигуратор класса
            $this->config = $linkEnsoConfig;
        }


        /*
         * Главный метод класса LinkEnso
         */
        public function run()
        {
            // Пробуем подгрузить содержимое модуля из кэша
            $output = false;
			if($this->dle_api->dle_config['allow_cache'] && $this->dle_api->dle_config['allow_cache'] != "no")
            {
                $output = $this->dle_api->load_from_cache('linkenso_'.md5(implode('_', $this->config)));
            }
            
            // Если значение кэша для данной конфигурации получено, выводим содержимое кэша
            if($output !== false)
            {
                $this->showOutput($output);
                return;
            }
            
            // Если в кэше ничего не найдено, генерируем модуль заново
            $wheres = array();
            
            // Получаем информацию о том, в каких категориях лежит данный пост
            $post = $this->dle_api->load_table (PREFIX."_post", 'category', 'id = '.$this->config['postId'], false);
            
            // Исправляем тупой косяк DLE API - такой метод ОБЯЗАН возвращать пустой массив в случае, если ничего не найдено
            if(!empty($post) && !empty($post['category'])) $postCategories = array();
            
            $postCategories = explode(',', $post['category']);
            
            // Получаем список категорий для выборки в зависимости от параметра scan
            $categoriesArray = array();
            switch($this->config['scan'])
            {
                // Если нужно сканировать только текущую категорию
                case 'same_cat':                    
                    // Каждую категорию текущего поста и все ее подкатегории добавляем в общий массив
                    foreach($postCategories as $postCategory)
                    {
                        $postCategory = intval($postCategory);
                        $categoriesArray[] = $postCategory;
                        $categoriesArray = array_merge($categoriesArray, $this->getSubcategoriesArray($postCategory));
                    }
                    break;
                
                // Если нужно сканировать все подкатегории самой "верхней категории"
                case 'global_cat':
                    // Для каждой из категорий текущего поста находим корневую категорию и все её подкатегории
                    foreach($postCategories as $postCategory)
                    {
                        $postCategory = intval($postCategory);
                        $globalCategoryId = $this->getGlobalCategory($postCategory);
                        $categoriesArray[] = $globalCategoryId;
                        $categoriesArray = array_merge($categoriesArray, $this->getSubcategoriesArray($globalCategoryId));
                    }
                    break;
                
                default:
                    break;
            }
            
            // Условие на список категорий
            if(count($categoriesArray) > 0)
            {
                switch($this->dle_api->dle_config['allow_multi_category'])
                {
                    // Если включена поддержка мультикатегорий
                    case '1':
                        $categoryWheres = array();
                        foreach($categoriesArray as $categoryId)
                        {
                            $categoryWheres[] = 'category regexp "[[:<:]]('.str_replace(',', '|', $categoryId).')[[:>:]]"';
                        }
                        $wheres[] = '('.implode(' OR ', $categoryWheres).')';
                        break;

                    // Если поддержки мультикатегорий нет
                    default:
                        $wheres[] = 'category IN ('.implode(',', $categoriesArray).')';
                        break;
                }               
            }
            
            // В зависимости от параметра date определяем старые нам посты нужны или новые
            switch($this->config['date'])
            {
                case 'new':
                    $dateWhere = 'id > '.$this->config['postId'];
                    break;
                
                default:
                    $dateWhere = 'id < '.$this->config['postId'];
                    break;
            }
            
            // Условие для отображения только постов, прошедших модерацию
            $wheres[] = 'approve = 1';

            // Условие для отображения только тех постов, дата публикации которых уже наступила
            $wheres[] = 'date < "'.date("Y-m-d H:i:s").'"';
            
            // Условие для фильтрации текущего id
            $wheres[] = 'id != '.$this->config['postId'];
            
            // Складываем условия
            $where = implode(' AND ', $wheres);
            
            // Направление сортировки зависит от того, свежие мы смотрим или старые (для свежих - ASC, для старых - DESC)
            $ordering = $this->config['date'] == 'new'?'ASC':'DESC';
            
            // Первый этап - получение предыдущих постов
            $postsSelect = 'id, title, metatitle, date, alt_name, category';
            $postsSelect .= $this->dle_api->dle_config['version_id'] < 9.6?', flag':''; // для старых dle выбираем поле flag
            $posts = $this->dle_api->load_table (PREFIX."_post", $postsSelect, $where.' AND '.$dateWhere, true, 0, $this->config['links'], 'id', $ordering);

            // Исправляем тупой косяк DLE API - такой метод ОБЯЗАН возвращать пустой массив в случае, если ничего не найдено
            if(empty($posts)) $posts = array();
            
            // Второй этап - если в нужном направлении постов не хватило и параметр ring установлен как 1, ищем посты с другой стороны
            if(count($posts) < $this->config['links'] && $this->config['ring'] == 'yes')
            {
                // Создаём список id постов, чтобы отфильтровать их
                $posts_id_array = array();
                foreach($posts as $post)
                {
                    $posts_id_array[] = $post['id'];
                }
                
                // Условие для фильтрации уже отобранных новостей
                if(!empty($posts_id_array))
                {
                    $wheres[] = 'id NOT IN('.implode(',', $posts_id_array).')';
                }
                
                // Складываем условия
                $where = implode(' AND ', $wheres);
                
                // Получаем доп. посты из новых
                $morePostsSelect = 'id, title, metatitle, date, alt_name, category';
                $morePostsSelect .= $this->dle_api->dle_config['version_id'] < 9.6?', flag':''; // для старых dle выбираем поле flag
                $morePosts = $this->dle_api->load_table (PREFIX."_post", $morePostsSelect, $where, true, 0, ($this->config['links'] - count($posts)), 'id', $ordering);
                
                // Исправляем тупой косяк DLE API - такой метод ОБЯЗАН возвращать пустой массив в случае, если ничего не найдено
                if(empty($morePosts)) $morePosts = array();
            
                $posts = array_merge_recursive($posts, $morePosts);
            }
            
            // Получаем вывод списка статей
            if(count($posts) > 0)
            {
                $output = '<ul>';
                foreach($posts as $post)
                {
                    
                    $output .= '<li><a '.($this->config['title'] != 'empty'?'title="'.($this->config['title'] == 'name'?stripslashes($post['title']):stripslashes($post['metatitle'])).'"':'').' href="'.($this->getPostUrl($post)).'">'.($this->config['anchor'] == 'title'?stripslashes($post['metatitle']):stripslashes($post['title'])).'</a></li>';
                }
                $output .= '</ul>';
            }
            
            // Если разрешено кэширование, сохраняем в кэш по данной конфигурации
			if($this->dle_api->dle_config['allow_cache'] && $this->dle_api->dle_config['allow_cache'] != "no")
            {
                $this->dle_api->save_to_cache('linkenso_'.md5(implode('_', $this->config)), $output);
            }
            
            // Выводим содержимое модуля
            $this->showOutput($output);
        }
        
        
        /*
         * Метод рекурсивно возвращает массив всех подкатегорий определенной категории
         * @param $categoryId - идентификатор исходной категории
         * @return array - массив со списком подкатегорий
         */
        public function getSubcategoriesArray($categoryId)
        {
            // Проверка $categoryId
            $categoryId = intval($categoryId);
            
            // Массив со списком подкатегорий
            $subcategoriesArray = array();
            
            // Получаем список подкатегорий
            $subcategories = $this->dle_api->load_table (PREFIX."_category", 'id', 'parentid = '.$categoryId, true);
            if(empty($subcategories)) $subcategories = array();
            
            foreach($subcategories as $subcategory)
            {
                // Добавляем в массив текущую подкатегорию
                $subcategoriesArray[] = intval($subcategory['id']);
                
                // Добавляем в массив все ее подкатегории
                $subcategoriesArray = array_merge($subcategoriesArray, $this->getSubcategoriesArray($subcategory['id']));
            }
            
            // Возвращаем массив подкатегорий
            return $subcategoriesArray;
        }

        
        /*
         * Метод возвращает массив всех подкатегорий определенной категории
         * @param $categoryId - идентификатор исходной категории
         * @return int - идентификатор самой "верхней" категории этой новости
         */
        public function getGlobalCategory($categoryId)
        {
            // Проверка $categoryId
            $categoryId = intval($categoryId);
            
            // Подхватываем глобальный массив с информацией о категориях
            global $cat_info;
            
            // Ползем по массиву категорий вверх, чтобы получить самую корневую категорию
            while($cat_info[$categoryId]['parentid'] > 0)
            {
                $categoryId = intval($cat_info[$categoryId]['parentid']);
            }
            
            // Возвращаем самую корневую категорию
            return $categoryId;
        }


        /*
         * @param $post - массив с информацией о статье
         * @return string URL для категории
         */
        public function getPostUrl($post)
        {
			if($this->dle_api->dle_config['allow_alt_url'] && $this->dle_api->dle_config['allow_alt_url'] != "no")
            {
                if(
                    ($this->dle_api->dle_config['version_id'] < 9.6 && $post['flag'] && $this->dle_api->dle_config['seo_type'])
                        ||
                    ($this->dle_api->dle_config['version_id'] >= 9.6 && ($this->dle_api->dle_config['seo_type'] == 1 || $this->dle_api->dle_config['seo_type'] == 2))
                )
                {
                    if(intval($post['category']) && $this->dle_api->dle_config['seo_type'] == 2)
                    {
                        $url = $this->dle_api->dle_config['http_home_url'].get_url(intval($post['category'])).'/'.$post['id'].'-'.$post['alt_name'].'.html';
                    }
                    else
                    {
                        $url = $this->dle_api->dle_config['http_home_url'].$post['id'].'-'.$post['alt_name'].'.html';
                    }
                }
                else
                {
                    $url = $this->dle_api->dle_config['http_home_url'].date("Y/m/d/", strtotime($post['date'])).$post['alt_name'].'.html';
                }
            }
            else
            {
                $url = $this->dle_api->dle_config['http_home_url'].'index.php?newsid='.$post['id'];
            }

            return $url;
        }
        
        
        /*
         * Метод подхватывает tpl-шаблон, заменяет в нём теги и возвращает отформатированную строку
         * @param $template - название шаблона, который нужно применить
         * @param $vars - ассоциативный массив с данными для замены переменных в шаблоне
         * @param $vars - ассоциативный массив с данными для замены блоков в шаблоне
         *
         * @return string tpl-шаблон, заполненный данными из массива $data
         */
        public function applyTemplate($template, $vars = array(), $blocks = array())
        {
            // Подключаем файл шаблона $template.tpl, заполняем его
            $tpl = new dle_template();
            $tpl->dir = TEMPLATE_DIR;
            $tpl->load_template($template.'.tpl');

            // Заполняем шаблон переменными
            foreach($vars as $var => $value)
            {
                $tpl->set($var, $value);
            }

            // Заполняем шаблон блоками
            foreach($blocks as $block => $value)
            {
                $tpl->set_block($block, $value);
            }

            // Компилируем шаблон (что бы это не означало ;))
            $tpl->compile($template);

            // Выводим результат
            return $tpl->result[$template];
        }


        /*
         * Метод выводит содержимое модуля в браузер
         * @param $output - строка для вывода
         */
        public function showOutput($output)
        {
            echo $output;
        }
    }}
    /*---End Of LinkEnso Class---*/
    
    
    // Подхватываем конфигурацию модуля
    $linkEnsoConfig = array(
        'postId'    => !empty($post_id)?$post_id:false,
        'links'     => !empty($links)?$links:3,
        'date'      => !empty($date)?$date:'old',
        'ring'      => !empty($ring)?$ring:'yes',
        'scan'      => !empty($scan)?$scan:'all_cat',
        'anchor'    => !empty($anchor)?$anchor:'name',
        'title'     => !empty($title)?$title:'title',
    );
    
    // Создаем экземпляр класса для перелинковки и запускаем его главный метод
    $linkEnso = new LinkEnso($linkEnsoConfig);
    $linkEnso->run();

?>