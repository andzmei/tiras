<?php
/*
=====================================================
 DataLife Engine - by SoftNews Media Group 
-----------------------------------------------------
 http://dle-news.ru/
-----------------------------------------------------
 Copyright (c) 2004,2011 SoftNews Media Group
=====================================================
 Данный код защищен авторскими правами
=====================================================
 Файл: topnews.php
-----------------------------------------------------
 Назначение: вывод рейтинговых статей
=====================================================


if( ! defined( 'DATALIFEENGINE' ) ) {
	die( "Hacking attempt!" );
}

$topnews2 = dle_cache( "topnews2", $config['skin'] );

if( $topnews2 === false ) {
	
	$this_month = date( 'Y-m-d H:i:s', $_TIME );
	
	$db->query( "SELECT id, title, date, alt_name, category, flag FROM " . PREFIX . "_post WHERE approve='1' and category=1120 ORDER BY rating DESC, comm_num DESC, news_read DESC, date DESC LIMIT 0,3" );
	
	while ( $row = $db->get_row() ) {
		
		$row['date'] = strtotime( $row['date'] );
		$row['category'] = intval( $row['category'] );
		
		if( $config['allow_alt_url'] == "yes" ) {
			
			if( $row['flag'] and $config['seo_type'] ) {
				
				if( $row['category'] and $config['seo_type'] == 2 ) {
					
					$full_link = $config['http_home_url'] . get_url( $row['category'] ) . "/" . $row['id'] . "-" . $row['alt_name'] . ".html";
				
				} else {
					
					$full_link = $config['http_home_url'] . $row['id'] . "-" . $row['alt_name'] . ".html";
				
				}
			
			} else {
				
				$full_link = $config['http_home_url'] . date( 'Y/m/d/', $row['date'] ) . $row['alt_name'] . ".html";
			}
		
		} else {
			
			$full_link = $config['http_home_url'] . "index.php?newsid=" . $row['id'];
		
		}
		
		if( dle_strlen( $row['title'], $config['charset'] ) > 55 ) $title = dle_substr( $row['title'], 0, 55, $config['charset'] ) . " ...";
		else $title = $row['title'];
		
		$link = "<a href=\"" . $full_link . "\">" . stripslashes( $title ) . "</a>";
		
		$topnews2 .= "<li>" . $link . "</li>";
	}
	
	$db->free();

	create_cache( "topnews2", $topnews2, $config['skin'] );
}
 * */

?>
