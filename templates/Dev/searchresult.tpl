<article class="news-r wrapper">

	[searchposts]

		[shortresult]

			<header>
				<div class="title">[result-link]{result-title}[/result-link]</div>
				<b>{result-date}</b> | {link-category} | Автор: {result-author} |
				[not-group=5][edit]Редактировать[/edit][/not-group]
			</header>

		[/shortresult]

		[fullresult]
		<header>
			<div class="img_fullstory"> [xfgiven_img] [xfvalue_img] [/xfgiven_img] </div>
			<div class="title">
				<h1>[result-link]{result-title}[/result-link]</h1>
				<p>{result-text}</p>
				</br>
				<div class="info">
					<div>{link-category} // {result-date} // <i class="far fa-eye"></i> {views} </div>
				</div>
			</div>
		</header>
		[/fullresult]

	[/searchposts]

</article>