<article class="news-r wrapper" >
    <header>
        <div class="img_fullstory"> [xfgiven_img] [xfvalue_img] [/xfgiven_img] </div>
        <div class="title"><h1><a href="{full-link}">{title}</a></h1>
            {short-story}
        <div class="info">
            <div>{category} // {date} // <i class="far fa-eye"></i> {views} </div>
        </div></div>
    </header>
</article>