<div class="razdel ">
    <article class="news-r wrapper wrapper-mobile">
        <header>
            <div class="img_fullstory">[xfgiven_img] [xfvalue_img] [/xfgiven_img]</div>
            <div class="title">
                <h1 >{title} </h1>
                <div class="info">
                    <div>{category} // {date} // <i class="far fa-eye"></i> {views} </div>
                    [edit] <i class="fas fa-pencil-alt"></i>[/edit]
                   <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                   <script src="//yastatic.net/share2/share.js"></script>
                   <div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,moimir" data-counter=""></div>
                </div>
            </div>
        </header>
        </article>

        <div class="full_content wrapper">{full-story}</div>
        <div class="relatednews_block wrapper">
            <h3><i class="fas fa-asterisk fa-spin fa-xs"></i> ДРУГОЕ ПО ТЕМЕ:</h3>
            <div class="wrap"> [related-news] {related-news} [/related-news] </div>

            <h3><i class="far fa-calendar-alt"></i> РАНЕЕ ПО РАЗДЕЛУ:</h3>
            <div class="wrap">
                {include file='engine/modules/linkenso.php?post_id={news-id}&links=3&date=old&ring=yes&scan=same_cat&anchor=name&title=title'}
                </div>
        </div>

        <div>{tags}</div>


        <div class="wrapper" id="disqus_thread"></div>
        <script>

        /**
        *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
        *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
        /*
        var disqus_config = function () {
        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        */
        (function() { // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        s.src = 'https://https-tiras-ru.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
        })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        <script id="dsq-count-scr" src="//https-tiras-ru.disqus.com/count.js" async></script>
        <!-- SAPE RTB DIV 240x400 -->
    <div id="SRTB_30302"></div>
    <!-- SAPE RTB END -->
</div>