<article>
	  [xfgiven_img]  [xfvalue_img]  [/xfgiven_img]  <!-- картинка -->
		<header>
				<div class="category">{link-category} 
					<i class="fas fa-angle-right"></i>
				</div>
				<h1 ><a  href="{full-link}">{title}</a></h1>
	</header>
	
	<main> 

	<span>{short-story}</span>
		<div class="info"> 
			<div>{link-category}</div> 
			<div>|</div>  
			<div class="data">{date}</div> 
			<div>|</div> 
			<div class="views"><i class="far fa-eye"></i> {views} </div>
		</div>
	 </div>
</article>