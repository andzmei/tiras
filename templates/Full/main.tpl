<!DOCTYPE html>
<html >

<head>
	<meta name="google-site-verification" content="NdECIiUdRG1wJNinidjfzC2JjdyO-WrFPorjuDF5ERY" />
	<meta name='wmail-verification' content='1f343e1361e9b9d8' />
	<meta name="msvalidate.01" content="333888C84BA40473DD3B42E87FEF3886" />
	<meta name='yandex-verification' content='f5f6f494e02d4e30' />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#000">
	{metatags}
	<link rel="stylesheet" href="/engine/classes/min/index.php?charset=UTF-8&amp;f={THEME}/style/style.css,{THEME}/style/engine.css&amp;7">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.css">
	<script async="async" src="//cdn-rtb.sape.ru/rtb-b/js/194/2/13194.js" type="text/javascript"></script>
	{headers} {AJAX}
</head>

<body>
	<header>
		<a class="logo" href="/"> <img src="{THEME}/images/logo.svg" alt="tisam.ru" > </a>
		<div class="intro">
			<div class="head_name">
				<!-- <img src="{THEME}/images/head_name.png" alt="/"> -->
				<div>НОВОСТНОЙ ПОРТАЛ СНГ</div>
				<div>События в политике, обществе, спорте. Сводка происшествий. Интервью </div> &nbsp <div> 2023</div>
			</div>
			<div class="block1">
				<div class="block1_wrapper">
					<span>
							<noindex><a href="http://gmail.com" rel="nofollow">iа.tirаs.ru@gmаil.соm</a></noindex>
							<span class="red"> /</span><span class="blue">/ </span><span>адрес редакции</span>
					</span>
				</div>
			</div>
		</div>

		<!-- главное меню -->
		<nav id="nav" role="navigation">
			<div class="title_mobile"> ИА «ТИРАС»</div>
			<a href="#nav" title="Show navigation">Show navigation</a>
			<a href="#" title="Hide navigation">Hide navigation</a>
			<ul>
				<li><a href="/"><i class="icon-fixed-width icon-home"></i> <i class="fas fa-home"></i> </a></li>
				<!-- <li><i data-eva="home-outline" data-eva-fill="#fff"></i></li> -->
				<li><a href="/tema-dnja/">ПОЛИТИКА</a></li>
				<li><a href="/biznes/">ЭКОНОМИКА</a></li>
				<li><a href="/v-mire/"><span>В МИРЕ</span></a>
					<ul>
						<li><a href="/evrazija/">ЕВРАЗИЯ</a></li>
					</ul>
				</li>
				<li><a href="/jeksperty/"><span>ЭКСПЕРТЫ</span></a>
					<ul>
						<li><a href="/honesty/">ИНТЕРВЬЮ</a></li>
					</ul>
				</li>
				<li><a href="/obshhestvo/"> <span>ОБЩЕСТВО</span> </a>
					<ul>
						<li><a href="/obrazovanie/">ОБРАЗОВАНИЕ</a></li>
						<li><a href="/religija/">РЕЛИГИЯ</a> </li>
						<li><a href="/voennoe/">ВОЕННОЕ</a> </li>
						<li><a href="/zdorove/">ЗДОРОВЬЕ</a> </li>
						<li><a href="/fashion/">МОДА</a> </li>
					</ul>
				</li>
				<li><a href="/kriminalnoe-chtivo/">СОБЫТИЯ</a></li>
				<li><a href="/sport/">СПОРТ</a></li>
				<!-- <form class="form1" action="" name="searchform" method="post">
					<input type="hidden" name="do" value="search" />
					<input type="hidden" name="subaction" value="search" />
					<input class="search" id="story" name="story" value="поиск по сайту" onblur="if (this.value == '')
											this.value = 'поиск по сайту';" onfocus="if (this.value == 'поиск по сайту')
											this.value = '';" type="text" />
					<div id="search"><i class="icon-search icon-large "></i></div>
				</form> -->
			</ul>
		</nav>
		</div>
		<!-- header end -->
		<!-- content -->
	</header>
	<main>
		[aviable=main]
			<header class="wrapper wrapper-mobile flex flex-wrap">
				{include file="actual_news.tpl"}
				<aside class="tools">
					{include file="aside_tools.tpl"}
				</aside>
			</header>
			<section class="actual_news_last wrapper">
				<h3>РАНЕЕ</h3>
				<ul> {include file="actual_news_last.tpl"} </ul>
			</section>
		[/aviable]

			<section class="content">

				[aviable=main]<section class="last_news">{include file="last_news.tpl"}</section>[/aviable]
				[aviable=showfull|cat|date|search]  {info}{content} [/aviable]
				[not-aviable=search|main|showfull|cat|conv|date] {info}{content} [/not-aviable]

				<aside>
					{include file="aside.tpl"}
				</aside>
			</section>

		[aviable=main]
			<div class="sape wrapper">
				{sape_links}
			</div>
			<div class="footer_news">
				<ul>
					<h3>ПОЛИТИКА</h3>{custom category="2" template="footer_news_iteam" aviable="main" from="0" limit="4" cache="no"}</ul>
				<ul>
					<h3>ЭКОНОМИКА</h3> {custom category="6" template="footer_news_iteam" aviable="main" from="0" limit="4" cache="no"}</ul>
				<ul>
					<h3>СОБЫТИЯ</h3>{custom category="1122" template="footer_news_iteam" aviable="main" from="0" limit="4" cache="no"}</ul>
				<ul style="margin-right:0px">
					<h3>ЭКСПЕРТЫ</h3>{custom category="4" template="footer_news_iteam" aviable="main" from="0" limit="4" cache="no"}</ul>
			</div>
			<!--<div class="bottom_banner" style="margin-right:18px">
					<noindex>
						<a href="http://tisam.ru"  rel="nofollow">
							<img src="{THEME}/images/school.png" alt="Школа развития:Ты сам">
						</a>
					</noindex >

					</div>
					<div class="bottom_banner">
						<span class="label1"><img  src="{THEME}/images/baner.jpg" alt="" /></span>
						<span class="label1"><img  src="{THEME}/images/baner.jpg" alt="" /></span>
				</div> -->
		[/aviable]

		<!-- [aviable=conv] {include file="engine/modules/conv.php"} [/aviable]  -->

	</main>
	<footer class="footer">
		<div class="footer_content"></div>
		<div class="copyright">
			<div id="banners" class="block-footer">
				<!-- Yandex.Metrika informer -->
				<a href="https://metrika.yandex.ru/stat/?id=21176020&amp;from=informer" target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/21176020/3_0_FFFFFFFF_FFFFFFFF_0_pageviews"
					 style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"
					 class="ym-advanced-informer" data-cid="21176020" data-lang="ru" /></a>
				<!-- /Yandex.Metrika informer -->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (m, e, t, r, i, k, a) {
						m[i] = m[i] || function () {
							(m[i].a = m[i].a || []).push(arguments)
						};
						m[i].l = 1 * new Date();
						k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
					})
						(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

					ym(21176020, "init", {
						id: 21176020,
						clickmap: true,
						trackLinks: true,
						accurateTrackBounce: true,
						webvisor: true
					});
				</script>
				<noscript>
					<div><img src="https://mc.yandex.ru/watch/21176020" style="position:absolute; left:-9999px;" alt="" /></div>
				</noscript>
				<!-- /Yandex.Metrika counter -->
				<!--LiveInternet counter-->
				<script type="text/javascript">
				document.write("<a href='http://www.liveinternet.ru/stat/tiras.ru/' " +
					"target=_blank ><img src='//counter.yadro.ru/hit?t17.6;r" +
					escape(document.referrer) + ((typeof(screen) == "undefined") ? "" :
						";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ?
							screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) +
					";" + Math.random() +
					"' alt='' title='LiveInternet: показано число просмотров за 24" +
					" часа, посетителей за 24 часа и за сегодня' " +
					"border='0' width='88' height='31'><\/a>")
				</script>
				<!--/LiveInternet-->

				<!-- Google tag (gtag.js) -->
				<script async src="https://www.googletagmanager.com/gtag/js?id=G-BV97QLYVEB"></script>
				<script>
					window.dataLayer = window.dataLayer || [];
					function gtag() { dataLayer.push(arguments); }
					gtag('js', new Date());

					gtag('config', 'G-BV97QLYVEB');
				</script>

			</div>
			<div class="copy">© ИА «Тирас», <a href="#skin" data-lity>2005-2023</a>
				<br>Кишинев - Тирасполь - Киев - Москва - Транзит
				<br>Перепечатка материалов обязательна со ссылкой на tiras.ru
				<br> </div>
			<div id="skin" class="lity-hide">xvxc {changeskin}</div>
		</div>
		</div>
	</footer>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.js"></script>
	<script src="https://unpkg.com/eva-icons@1.1.1/eva.min.js"></script>
	<script>
		eva.replace()
	</script>
	<script type="text/javascript">
	$(document).ready(function() {
		$(window).scroll(function() {
			if ($(this).scrollTop() > 0) {
				$('#scroller').fadeIn();
			} else { $('#scroller').fadeOut(); }
		});
		$('#scroller').click(function() { $('body,html').animate({ scrollTop: 0 }, 400); return false; });
	});
	</script>
	<div id="scroller" class="b-top" style="display: none;">
		<span class="b-top-but"><i class="fas fa-arrow-circle-up"></i></span>
	</div>
</body>

</html>