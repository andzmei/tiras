<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <meta name="google-site-verification" content="NdECIiUdRG1wJNinidjfzC2JjdyO-WrFPorjuDF5ERY" />
        <meta name='wmail-verification' content='1f343e1361e9b9d8' />
		<meta name="msvalidate.01" content="333888C84BA40473DD3B42E87FEF3886" />
		
	<!-- 	<link media="screen" href="http://dlyaokon.ru/tiras/style/style2.css" type="text/css" rel="stylesheet" />  --> 
        <link media="screen" href="{THEME}/style/style2.css" type="text/css" rel="stylesheet" />
         
        <link media="screen" href="{THEME}/style/engine.css" type="text/css" rel="stylesheet" />
		
        {headers}
    
        <script type="text/javascript" src="{THEME}/js/libs.js"></script> 


        <script type="text/javascript">$(document).ready(function() {
                        $(window).scroll(function() {if ($(this).scrollTop() > 0) {
						$('#scroller').fadeIn();} else {$('#scroller').fadeOut();}}
                        );
                        $('#scroller').click(function() {$('body,html').animate({scrollTop: 0}, 400); return false;}
                        );
                    });
        </script>

    </head>

    <body>
	        {AJAX}
        
        <div class="header">

            <div id="wraper">
            <a href="/"><div class="logo"></div></a> 
                <a href="http://tisam.ru"><div class="top-news">
                        <div class="top-text"></div>
                    </div></a>


                <div class="top-menu">
                    <a href="/tema-dnja/">ТЕМА ДНЯ!</a> | 
                    <a href="/jeksperty/">Эксперты</a> · 
                    <a href="/v-mire/">В мире</a> · 
                    <a href="/evrazija/">Евразия</a> · 
                    <a href="/obshhestvo/">Общество</a> · 
                    <a href="/biznes/">Бизнес</a> · 
                    <a href="/voennoe/">Военное</a> · 
                    <a href="/obrazovanie/">Образование</a> · 
                    <a href="/religija/">Религия</a> · 
                    <a href="/zdorove/">Здоровье</a> ·
                    <a href="/sport/">Спорт</a> · 
                    <a href="/kriminalnoe-chtivo/">Криминал</a><a href="/ttlnews/"></a>
                </div>

            </div>
        </div>
        <!-- header end -->

        <!-- content -->
		
		
		
        <div class="content"> 
		<div class="sape">{sape_links}</div> 
		
		
            <div class="news">


                [aviable=main]

               {include file="actual_news.tpl"}
                {cat_news}  

                [/aviable]



                [not-aviable=main] 
                {info}{content} 
                [/not-aviable]
				
						
	

						
            </div>

            <div class="block">

                <div class="title-b"><a href="/en/">English version</a></div>
                <div class="content-b">

                </div>
                
                <br>
                <br>
				<div class="title-b">Популярные новости</div>
                            <div class="content-b">
                                <ul>
                                    {topnews}
                                </ul>
                            </div>
							 <br>
							
                    <div class="title-b">Поиск:</div>
                    <div class="content-b">
                        <form action="" name="searchform" method="post">
                            <input type="hidden" name="do" value="search" />
                            <input type="hidden" name="subaction" value="search" />
                            <!--<ul class="searchbar reset">-->
                            <!--<li class="lfield">--><input id="story" name="story" value="Поиск..." onblur="if (this.value == '')
                            this.value = 'Поиск...';" onfocus="if (this.value == 'Поиск...')
                            this.value = '';" type="text" /></li>
                            <!--<li class="lbtn"><input title="Найти" alt="Найти" type="image" src="{THEME}/images/spacer.gif" /></li>
                          </ul>-->
                        </form>
                    </div>
                    <br>
                        <div class="title-b">Календарь:</div>
                        <div class="content-b">
                            {calendar}
                        </div>
                        <br>
                           <div class="title-b">Наш опрос</div>
							<div class="content-b">{vote}</div>  


                            <br><br><br><br><br>
                                 
                                     </div>
                                 </div>
                                <!-- content end -->

                                                <!-- footer -->
                                                <div class="footer">
												
												
												<div style="margin: 4px 7px 3px 15px;">
												
												<!-- Yandex.Metrika informer -->
													<a href="http://metrika.yandex.ru/stat/?id=21176020&amp;from=informer"
													target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/21176020/3_0_FFFFFFFF_FFFFFFFF_0_pageviews"
													style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:21176020,lang:'ru'});return false}catch(e){}"/></a>
													<!-- /Yandex.Metrika informer -->

													<!-- Yandex.Metrika counter -->
													<script type="text/javascript">
													(function (d, w, c) {
														(w[c] = w[c] || []).push(function() {
															try {
																w.yaCounter21176020 = new Ya.Metrika({id:21176020,
																		webvisor:true,
																		clickmap:true,
																		trackLinks:true,
																		accurateTrackBounce:true});
															} catch(e) { }
														});

														var n = d.getElementsByTagName("script")[0],
															s = d.createElement("script"),
															f = function () { n.parentNode.insertBefore(s, n); };
														s.type = "text/javascript";
														s.async = true;
														s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

														if (w.opera == "[object Opera]") {
															d.addEventListener("DOMContentLoaded", f, false);
														} else { f(); }
													})(document, window, "yandex_metrika_callbacks");
													</script>
													<noscript><div><img src="//mc.yandex.ru/watch/21176020" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
													<!-- /Yandex.Metrika counter -->
												
												
												</div>
												
												
                                                 <div class="copy">© ИА «Тирас», 2005-2013<br>Кишинев - Тирасполь - Киев - Москва - Транзит<br>Перепечатка материалов обязательна со ссылкой на tiras.ru</div> 
                                                                
																  [group=1] {changeskin} [/group]
                                                                <div class="adress"><br>Адрес редакции: Norway, Oslo<br><a href="/index.php?do=feedback">oslofree@gmail.com</a></div>
                                                                            </div>
                                                                            <!-- footer end -->

                                                                            <div id="scroller" class="b-top" style="display: none;"><span class="b-top-but">наверх</span></div>

                                                                            </body>
                                                                            </html>